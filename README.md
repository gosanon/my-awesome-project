# Task 1
## Help message
```python
python main.py -h
```
OR
```python
python main.py --help
```
## Example usage
```
python main.py 173.194.222.139
```
*(ip instead of domain name is also supported)*

Will print:

```
IP              ASN     COUNTRY CITY
192.168.1.1     Local   -       -
213.242.203.32  3253    RU      Yekaterinburg
195.58.0.158    3253    RU      Yekaterinburg
87.254.133.197  3216    RU      Yekaterinburg
79.104.225.13   NA      RU      Moscow
81.211.45.63    3216    RU      Moscow
79.104.235.215  NA      RU      Moscow
72.14.205.76    15169   US      Moscow
108.170.250.130 15169   US      Moscow
142.251.238.84  15169   US      Fremont
142.251.238.72  15169   US      Fremont
172.253.51.189  15169   US      Fremont
***.***.***.***         -       -       -
***.***.***.***         -       -       -
***.***.***.***         -       -       -
***.***.***.***         -       -       -
***.***.***.***         -       -       -
***.***.***.***         -       -       -
***.***.***.***         -       -       -
***.***.***.***         -       -       -
***.***.***.***         -       -       -
173.194.222.139 15169   US      Fremont
```