import argparse
import asyncio
from functools import partial, wraps
import subprocess
import time
import ipwhois
import re
import json
from urllib.request import urlopen


def async_wrapper(func):
    @wraps(func)
    async def run(*args, loop=None, executor=None, **kwargs):
        if loop is None:
            loop = asyncio.get_event_loop()
        pfunc = partial(func, *args, **kwargs)
        return await loop.run_in_executor(executor, pfunc)
    return run

@async_wrapper
def async_whois(ip):
    try:
        return ipwhois.IPWhois(ip).lookup_whois()
    except ipwhois.exceptions.IPDefinedError:
        # "IPv4 address 198.18.9.36 is already defined as Network Interconnect Device Benchmark Testing via RFC 2544."
        return { 'asn': '-', 'nets': [{ 'country': '-' }] }


possible_provider_replacing_re = re.compile(r'^AS\d* ')
@async_wrapper
def extended_info(ip):
    url = f'http://ipinfo.io/{ ip }/json'
    res = json.load(urlopen(url))

    possible_provider = '-'
    try:
        possible_provider = re.sub(possible_provider_replacing_re, '', res['org'])
    except:
        pass

    return possible_provider


async def as_info_and_geo(ip):
    res = await async_whois(ip)
    
    return f'{ res["asn"] }\t{ res["nets"][0]["country"] }\t{ await extended_info(ip) }'


local_ip_re = re.compile(r"192\.168\.\d{1,3}\.\d{1,3}")
ip_re = re.compile(r"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
miss_re = re.compile(r"\* {8}\* {8}\*")

async def process(line):
    decoded_line = line.decode('CP866')
    match_local_ip = re.findall(local_ip_re, decoded_line)
    match_ip = re.findall(ip_re, decoded_line)
    match_miss = re.findall(miss_re, decoded_line)

    if match_local_ip:
        return f"{ match_ip[0] }\tLocal\t-\t-"
    elif match_ip:
        return f"{ match_ip[-1] }\t{ await as_info_and_geo(match_ip[-1]) }"
    elif match_miss:
        return f"***.***.***.***\t-\t-"


async def main(address):
    t1 = time.time()
    tracert_result = None
    try:
        print('Выполнение команды tracert...')
        tracert_result = subprocess.check_output(["tracert", address]).splitlines()
    except subprocess.CalledProcessError:
        print(f'Error: tracert could not resolve address "{ address }"')
        exit(0)

    print(f'ID\tIP\t\tAS\tCOUNTRY\tPROVIDER')
    counter = 1

    results = await asyncio.gather(*map(process, tracert_result[2:]))
    
    for result in results:
        if result is not None:
            print(f'{counter}\t{result}')
            counter += 1
    t2 = time.time()
    print()
    print(t2-t1, 'seconds elapsed.')


def prepare_args():
    parser = argparse.ArgumentParser(description="Tracerouter in the name of Eldar Zharakhov")
    parser.add_argument("target", help="IP address or domain name")
    return parser.parse_args()

if __name__ == '__main__':
    args = prepare_args()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(args.target))
